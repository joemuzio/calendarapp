import { PromotionalActivityPage } from './app.po';

describe('promotional-activity App', () => {
  let page: PromotionalActivityPage;

  beforeEach(() => {
    page = new PromotionalActivityPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
