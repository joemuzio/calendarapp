/**
 * Created by jmuzio on 4/5/17.
 */
import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ForecastCalendarComponent } from './forecast-calendar/forecast-calendar.component'
import { ActiveOffersComponent } from './active-offers/active-offers.component'
// import { NewUserComponent } from './new-user/new-user.component';

import { AuthGuard } from './auth.guard';

export const routes: Routes = [
  { path: '', redirectTo: 'home/forecast', pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'login',  component: LoginComponent },
  { path: 'home',   component: HomeComponent, canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'forecast', pathMatch: 'full' },
      { path: 'forecast', component: ForecastCalendarComponent, canActivate: [AuthGuard] },
      { path: 'actual', component: ActiveOffersComponent, canActivate: [AuthGuard] },
      // { path: 'user-setup', component: NewUserComponent, canActivate: [AuthGuard] }
    ]},
  { path: '**',     component: HomeComponent, canActivate: [AuthGuard]}
];

