/**
 * Created by jmuzio on 4/5/17.
 */
import {Http, Response, Headers} from '@angular/http';
import {Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import {PromoHierarchyItem} from './app.models';
import { environment } from '../environments/environment';


const authURL = environment.host + '/RetailerPortal/oauth/token';
const narHierarchyURL = environment.host + '/RetailerPortal/reports/getCedw4HierarchyForRetailer/';
const narForecastURL = environment.host + '/RetailerPortal/reports/getCedw4DataForRetailer/';
const narForecastURLUK = environment.host + '/RetailerPortal/reports/getNarForecastDataEU/';
const loginHeaders = new Headers({'accept': 'application/json', 'content-type': 'application/x-www-form-urlencoded',
  'Authorization': 'Basic ' + btoa('rp_app:rp_app')});

@Injectable()
export class WebserviceController {
  userObj = {
    userName: "",
    token: "",
    retailer: "",
  };

  constructor(private http: Http) {
    this.userObj.userName = this.getUserName();
    this.userObj.token = this.getToken();
    this.userObj.retailer = this.getRetailerName();
  }
 
  private handleError(error: Response) {
    return Observable.throw(error.statusText);
  }

  private getTokenHeaders(): Headers{
    var tokenHeaders = new Headers();
    const authToken: string = this.userObj.token;
    tokenHeaders.append('Authorization', 'Bearer ' + authToken);
    return tokenHeaders
  }

  public login(username, password): Observable<any> {
    let body = 'grant_type=password&username=' + username + '&password=' + encodeURIComponent(password);
    return this.http.post(authURL, body, {headers: loginHeaders})
      .timeout(5000)
      .map(res => (res.json()));
  }

  public getUserData(): Observable<any>{
    let tokenURL = environment.host + "/RetailerPortal/user/getUserDetails/" + this.userObj.userName;
    return this.http.get(tokenURL, {headers: this.getTokenHeaders()} ).map((res:Response) => res.json());
  }

  public getNARDistributionURLAndToken(): Observable<any>{
    let userData = this.getUserData();
    let mstrToken = this.getMSTRToken();
    return Observable.forkJoin([userData, mstrToken]);
  }

  public getPromoHierData(): Observable<PromoHierarchyItem[]> {
    return this.http.get(narHierarchyURL, {headers: this.getTokenHeaders()} ).map(
      (res:Response) => <PromoHierarchyItem[]>res.json()
    ).catch(this.handleError);
  }

  public getNARForecastData(): Observable<any> {
    return this.http.get(narForecastURL, {headers: this.getTokenHeaders()} ).map((res:Response) => res.json())
  }

  public getNARForecastDataUK(): Observable<any> {
    return this.http.get(narForecastURLUK, {headers: this.getTokenHeaders()} ).map((res:Response) => res.json())
  }

  public getMSTRToken(): Observable<any> {
    let tokenURL = environment.host + "/RetailerPortal//secure/getMSTRTokenForUser/" + this.userObj.userName;
    return this.http.get(tokenURL, {headers: this.getTokenHeaders()} ).map((res:Response) => res.json())
  }

  private getUserName():string{
    if (localStorage.getItem('user')){
      return localStorage.getItem('user');
    } else { 
      return ""; 
    }
  }

  private getToken():string{
    if(localStorage.getItem('token')){
      return localStorage.getItem('token')
    } else {
      return ""; 
    }
  }

  public getRetailerName():string{
    if(localStorage.getItem('retailer')){
      return localStorage.getItem('retailer')
    } else {
      return ""; 
    }
  }

  public setUserName(userName:string){
    this.userObj.userName = userName;
    localStorage.setItem('user', userName);
  }

  public setToken(token:string){
    this.userObj.token = token;
    localStorage.setItem('token', token);
  }

  public setRetailerName(retailer: string){
    this.userObj.retailer = retailer;
    localStorage.setItem('retailer', retailer);
  }

  public removeUserName(){ this.setUserName(""); }
  public removeToken(){ this.setToken(""); }
  public removeRetailerName(){ this.setRetailerName(""); }
}
