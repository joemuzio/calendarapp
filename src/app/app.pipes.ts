import {Pipe, PipeTransform} from '@angular/core';
import {PromoHierarchyItem} from './app.models';

@Pipe({
        name:'HierCodeFilter',
        pure:false
})
export class HierCodeFilterPipe implements PipeTransform {
    transform(items: any[], filter: string, hierLevel: number = 1):any {
        if(!items || !filter) return items;
        if(hierLevel>5) hierLevel=5;
        if(hierLevel<1) hierLevel=0;
        let labels:string[]=["l1_hier_cd","l2_hier_cd","l3_hier_cd","l4_hier_cd","l5_hier_cd"];
        let nm:string=labels[hierLevel];
        return items.filter(item => item[nm]=filter);
    }
}

@Pipe({
    name:'HierDescFilter',
    pure:false
})
export class HierDescFilterPipe implements PipeTransform {
    transform(items: any[], filter: string, hierLevel: number = 1):any {
        if(!items || !filter) return items;
        if(hierLevel>5) hierLevel=5;
        if(hierLevel<1) hierLevel=0;
        let labels:string[]=["l1_hier_desc","l2_hier_desc","l3_hier_desc","l4_hier_desc","l5_hier_desc"];
        let nm:string=labels[hierLevel];
        return items.filter(item => item[nm]=filter);
    }
}
