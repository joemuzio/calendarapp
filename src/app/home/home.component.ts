import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {WebserviceController} from "../WebserviceController";
import {DomSanitizer} from "@angular/platform-browser";
import {AnalyticsService} from "../AnalyticsService";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  logoURL = "";
  showAllLinks = false;
  constructor(private router: Router, private webservices:WebserviceController, private analyticsService: AnalyticsService, public sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.webservices.getUserData().subscribe(response => {
        if(response.retailer.retailerName){
          var retailer = response.retailer.retailerName;
          if(response.isExternal != null && (response.isExternal == "N" || response.isExternal == "n")){
            retailer = "Internal_" + response.retailer.retailerName;
          }
          this.analyticsService.sendRetailerName(retailer);
        }
        if(response.retailer.id != 12){
          this.showAllLinks = true;
        }
        if(response.retailer.retailerImage){
          this.logoURL = response.retailer.retailerImage;
        }
      },
      err => {
        alert(err)
      });
  }
  logout() {
    localStorage.removeItem('id_token');
    this.router.navigate(['login']);
  }
}
