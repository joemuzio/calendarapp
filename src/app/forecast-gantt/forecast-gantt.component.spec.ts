import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastGanttComponent } from './forecast-gantt.component';

describe('ForecastGanttComponent', () => {
  let component: ForecastGanttComponent;
  let fixture: ComponentFixture<ForecastGanttComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastGanttComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastGanttComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
