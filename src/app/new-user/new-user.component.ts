import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder }  from '@angular/forms';
import {isSuccess} from "@angular/http/src/http_utils";

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css'],

})
export class NewUserComponent implements OnInit {
  emailExpression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  phoneExpression = /(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)([2-9]1[02-9]‌​|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})\s*(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+)\s*)?$/i;

  userForm: FormGroup;
  user = {firstName: "", lastName: "", email: "", phone: "", role: ""};
  active = true;
  alerts = [];
  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.buildForm();
  }

  onSubmit(){
    this.user = {firstName: "", lastName: "", email: "", phone: "", role: ""};
    this.buildForm();
    this.alerts.push({
      type: 'success',
      msg: 'New user request was successfully received.',
      timeout: 3000
    });
  }

  buildForm(): void {
    this.userForm = this.fb.group({
      firstName: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(24)
      ]],
      lastName: ['', [
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(24)
      ]],
      email: ['', [
        Validators.required,
        Validators.pattern(this.emailExpression),
      ]],
      phone: ['', [
        Validators.required,
        Validators.pattern(this.phoneExpression),
      ]],
      role: ['', [
        Validators.required,
      ]]
    });
    this.userForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // (re)set validation messages now
  }
  onValueChanged(data?: any) {
    if (!this.userForm) { return; }
    this.user.email = this.user.email.match(new RegExp('.{1,4}$|.{1,3}', 'g')).join("-");
    const form = this.userForm;
    for (const field in this.formErrors) {
      // clear previous error message (if any)
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }

  formErrors = {
    'firstName': '',
    'lastName': '',
    'email': '',
    'phone': '',
    'role': ''
  };
  validationMessages = {
    'firstName': {
      'required':      'First name is required.',
      'minlength':     'First name must be at least 4 characters long.',
      'maxlength':     'First name cannot be more than 24 characters long.'
    },
    'lastName': {
      'required':      'Last name is required.',
      'minlength':     'Last name must be at least 4 characters long.',
      'maxlength':     'Last name cannot be more than 24 characters long.'
    },
    'email': {
      'required':      'Email is required.',
      'pattern':       'Email is not valid.'
    },
    'phone': {
      'required':      'Phone number is required.',
      'pattern':       'Phone number is not valid.'
    },
    'role': {
      'required':      'Role is required.',
    }
  };
}

