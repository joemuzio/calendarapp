import {Component, OnInit, ViewChild} from '@angular/core';
import {ModalDirective} from "ngx-bootstrap";

const retailerNotAssignedError = "Please contact your sales representative for access.";
const noNarIDFoundError = "Document not found. Please contact your sales representative.";
const badCredentialsError = "Invalid credentials. Please try again.";
const networkError = "Network issue please try again. If problem persists please contact support.";
const iosVersionTooLow = "iOS 10 or greater is required to access this site.";

@Component({
  selector: 'app-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.css']
})
export class ErrorComponent implements OnInit {

  @ViewChild('autoShownModal') public autoShownModal:ModalDirective;
  isModalShown:boolean = false;
  errorMessage = "";

  constructor() { }

  ngOnInit() {

  }
  getErrorMessage(error): string{
    let errorStatusCode = error.status;
    if(errorStatusCode >= 400 && errorStatusCode <= 499)return badCredentialsError;
    else if(errorStatusCode >= 500 && errorStatusCode <= 599 || !errorStatusCode)return networkError;
    else {return networkError}
  }
  getRetailerNotAssignedError():string{
    return retailerNotAssignedError;
  }
  getNoNarIDError():string{
    return noNarIDFoundError;
  }
  getiOSVersionError():string{
    return iosVersionTooLow;
  }

  showErrorModalForError(error){
    this.errorMessage = this.getErrorMessage(error);
    this.showModal();
  }
  showErrorModalForNoRetailerAssigned(){
    this.errorMessage = this.getRetailerNotAssignedError();
    this.showModal();
  }
  showErrorModalNoNARIDFound(){
    this.errorMessage = this.getNoNarIDError();
    this.showModal();
  }
  showiOSTooLowModal(){
    this.errorMessage = this.getiOSVersionError();
    this.showModal();
  }


  public showModal():void {
    this.isModalShown = true;
  }

  public hideModal():void {
    this.autoShownModal.hide();
  }

  public onHidden():void {
    this.isModalShown = false;
    this.errorMessage = "";
  }
}
