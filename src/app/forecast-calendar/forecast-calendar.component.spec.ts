import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForecastCalendarComponent } from './forecast-calendar.component';

describe('ForecastCalendarComponent', () => {
  let component: ForecastCalendarComponent;
  let fixture: ComponentFixture<ForecastCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForecastCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
