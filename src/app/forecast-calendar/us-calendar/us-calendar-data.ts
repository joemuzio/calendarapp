/**
 * Created by jmuzio on 4/5/17.
 */
import {USCalendarDataModel} from "./us-calendar-data-model";

export class USCalendarData {
  public containsManufacturer(manufacturer: string, manufacturersArray:Array<any>): boolean{
    let doesContain = false;
    for (var i = 0; i < manufacturersArray.length; i++){
      let manufacturerString = manufacturersArray[i].id;
      if (manufacturer == manufacturerString){ doesContain = true; }
    }
    return doesContain;
  }
  public containsOfferType(offerType:string, offerTypesArray:Array<any>): boolean{
    let doesContain = false;
    for (var i = 0; i < offerTypesArray.length; i++){
      let offerString = offerTypesArray[i].id;
      if (offerType == offerString){ doesContain = true; }
    }
    return doesContain;
  }
  public containsCategory(category: string, categoriesArray:Array<any>): boolean{
    let doesContain = false;
    for (var i = 0; i < categoriesArray.length; i++){
      let categoryString = categoriesArray[i].id;
      if (category == categoryString){ doesContain = true; }
    }
    return doesContain;
  }
  private containsSource(source:string, array:Array<string>){
    let doesContain = false;
    for (var i = 0; i < array.length; i++){
      let sourceString = array[i];
      if (source == sourceString){ doesContain = true; }
    }
    return doesContain;
  }
  private containsMCLU(mclu:string, array):boolean{
    for (var i = 0; i < array.length; i++){
      let offerToCheck = array[i];
      if (offerToCheck.mclu == mclu){
        return true;
      }
    }
    return false;
  }


  sameDaySameManufacturer(event:USCalendarDataModel, array:Array<any>):boolean{
    for(var i = 0; i < array.length; i++){
      let compareEvent = array[i];
      if (this.sameDay(event, compareEvent)){
        if(compareEvent.manufacturer == event.manufacturer){
          return true;
        }
      }
    }
    return false
  }
  sameDay(event1:USCalendarDataModel, event2:USCalendarDataModel):boolean{
    if(event1.start.getFullYear() == event2.start.getFullYear() &&
      event1.start.getMonth() == event2.start.getMonth() &&
      event1.start.getDay() == event2.start.getDay()){
      return true;
    }
    return false;
  }

  public getDataObjects(offersArray:Array<any>):Array<any>{
    var newEvents = [];
    for (var i = 0; i < offersArray.length; i++){
      let event = offersArray[i];
      var containsMCLU = this.containsMCLU(event.mclu, newEvents);
      if (containsMCLU == false && event.isActive){
        newEvents.push(event);
      }
    }
    return newEvents;
  }
  filterManufacturerByStartDay(dataArray:Array<any>): Array<any>{
    let newData = [];
    for (var i = 0; i < dataArray.length; i++){
      let event = dataArray[i];
      if(this.sameDaySameManufacturer(event, newData) == false){
        newData.push(event);
      }
    }
    return newData;
  }


  filterManufacturer(dataArray:Array<any>, selectedManufacturers:Array<any>): Array<any>{
    if(selectedManufacturers.length > 0) {
      var newData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let obj = dataArray[i];
        for (var j = 0; j < selectedManufacturers.length; j++) {
          let manufacturer = selectedManufacturers[j];
          if (manufacturer.id == obj.manufacturer) {
            newData.push(obj);
          }
        }
      }
      return newData;
    }
    else{
      return dataArray;
    }
  }
  filterOfferTypes(dataArray:Array<any>, selectedOfferTypes:Array<any>): Array<any>{
    if(selectedOfferTypes.length > 0) {
      var newData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let obj = dataArray[i];
        for (var j = 0; j < selectedOfferTypes.length; j++) {
          let offer = selectedOfferTypes[j];
          if (offer.id == obj.couponType) {
            newData.push(obj);
          }
        }
      }
      return newData;
    }
    else{
      return dataArray;
    }
  }
  filterCategories(dataArray:Array<any>, selectedCategories:Array<any>):Array<any>{
    if(selectedCategories.length > 0) {
      var newData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let obj = dataArray[i];
        for (var j = 0; j < selectedCategories.length; j++) {
          let cat = selectedCategories[j];
          if (cat.id == obj.category) {
            newData.push(obj);
          }
        }
      }
      return newData;
    }
    else{
      return dataArray;
    }
  }
  filterSource(dataArray:Array<any>, radioModel: string):Array<any>{
    if (radioModel != "All") {
      var newData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let obj = dataArray[i];
        if (radioModel == "Retailer"){
          if (obj.source == "Retailer"){
            newData.push(obj);
          }
        }
        else if(radioModel == "Manufacturer"){
          if (obj.source == "Manufacturer"){
            newData.push(obj);
          }
        }
        else if(radioModel == "Loyalty"){
          if (obj.source == "Loyalty"){
            newData.push(obj);
          }
        }
      }
      return newData;
    }
    else{
      return dataArray;
    }
  }

  getAvailableSources(dataArray:Array<any>){
    var sourceArray = [];
    for (var i = 0; i < dataArray.length; i++){
      let source = dataArray[i].source;
      if(this.containsSource(source, sourceArray) == false){
        sourceArray.push(source);
      }
    }
    //maintain order
    let orderedArray = [];
    if(sourceArray.length > 1) {
      orderedArray.push("All");
      if (this.containsSource("Manufacturer", sourceArray)) {
        orderedArray.push("Manufacturer");
      }
      if (this.containsSource("Retailer", sourceArray)) {
        orderedArray.push("Retailer");
      }
      if (this.containsSource("Loyalty", sourceArray)) {
        orderedArray.push("Loyalty");
      }
    }
    return orderedArray;

  }
  getCategoryArray(dataArray:Array<any>, radioModel:string, selectedManufacturers:Array<any>, selectedOfferTypes:Array<any>):Array<any>{
    var categoryArray = [];
    dataArray = this.filterSource(dataArray, radioModel);
    dataArray = this.filterManufacturer(dataArray, selectedManufacturers);
    dataArray = this.filterOfferTypes(dataArray, selectedOfferTypes);
    for (var i = 0; i < dataArray.length; i++){
      let offer = dataArray[i];
      if (this.containsCategory(offer.category, categoryArray) == false) {
        let categoryObj = {
          id: offer.category,
          text: '<span style="pointer-events:none">' + offer.category + '</span>'
        };
        categoryArray.push(categoryObj);
      }
    }
    categoryArray.sort(function(a, b) {
      var textA = a.text.toUpperCase();
      var textB = b.text.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    return categoryArray;
  }
  getManufacturersArray(dataArray:Array<any>, radioModel:string, selectedCategories:Array<any>, selectedOfferTypes:Array<any>):Array<any>{
    dataArray = this.filterSource(dataArray, radioModel);
    dataArray = this.filterCategories(dataArray, selectedCategories);
    dataArray = this.filterOfferTypes(dataArray, selectedOfferTypes);
    var manufacturerArray = [];
    for (var i = 0; i < dataArray.length; i++){
      let offer = dataArray[i];
      if (this.containsManufacturer(offer.manufacturer, manufacturerArray) == false) {
        let manufacturerObj = {
          id: offer.manufacturer,
          text: '<span style="pointer-events:none">' + offer.manufacturer + '</span>'
        };
        manufacturerArray.push(manufacturerObj);
      }
    }
    manufacturerArray.sort(function(a, b) {
      var textA = a.text.toUpperCase();
      var textB = b.text.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    return manufacturerArray;
  }
  getOfferTypesArray(dataArray:Array<any>, radioModel:string, selectedCategories:Array<any>, selectedManufacturers:Array<any>):Array<any>{
    dataArray = this.filterSource(dataArray, radioModel);
    dataArray = this.filterCategories(dataArray, selectedCategories);
    dataArray = this.filterManufacturer(dataArray, selectedManufacturers);
    var offerTypesArray = [];
    for (var i = 0; i < dataArray.length; i++){
      let offer = dataArray[i];
      if (this.containsOfferType(offer.couponType, offerTypesArray) == false) {
        let offerTypeObj = {
          id: offer.couponType,
          text: '<span style="pointer-events:none">' + offer.couponType + '</span>'
        };
        offerTypesArray.push(offerTypeObj);
      }
    }
    offerTypesArray.sort(function(a, b) {
      var textA = a.text.toUpperCase();
      var textB = b.text.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    return offerTypesArray;
  }
}
