import {OnInit, Component, Input, ViewChild} from '@angular/core';
import { CalendarEvent, CalendarMonthViewDay } from 'angular-calendar';
import {subMonths, addMonths, addDays, addWeeks, subDays, subWeeks} from 'date-fns';
import {WebserviceController} from "../../WebserviceController";
import { ModalDirective } from 'ngx-bootstrap/modal';
import {USCalendarData} from "./us-calendar-data";
import {USCalendarTableData} from "./us-calendar-table-data";
import {USCalendarDataModel} from "./us-calendar-data-model";
import {Subscription} from 'rxjs';
import {ErrorComponent} from "../../error/error.component";
import {AnalyticsService} from "../../AnalyticsService";
import {DomSanitizer} from "@angular/platform-browser";
import { environment } from '../../../environments/environment';

type CalendarPeriod = 'day' | 'week' | 'month';

function addPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: addDays,
    week: addWeeks,
    month: addMonths
  }[period](date, amount);
}

function subPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: subDays,
    week: subWeeks,
    month: subMonths
  }[period](date, amount);
}
function getMCLUBaseURL(){
  if(environment.production){
    return "https://insights-service.catalinamarketing.com/BIImageService/imageservice/imagebyparentcorp?country=US&mclu=";
  }
  else{
    return "https://uatinsights.catalina.com/BIImageService/imageservice/imagebyparentcorp?country=US&mclu=";
  }
}
interface MyEvent extends CalendarEvent {
  manufacturer: string;
  source:string;
  color: {primary: string, secondary: string}
}
@Component({
  selector: 'app-us-calendar',
  templateUrl: './us-calendar.component.html',
  styleUrls: ['./us-calendar.component.css']
})
export class UsCalendarComponent implements OnInit {
  @ViewChild('childModal') public childModal:ModalDirective;
  @ViewChild(ErrorComponent) public errorComponent: ErrorComponent;
  public forecastData = new USCalendarData();
  public forecastTableData = new USCalendarTableData();
  view: CalendarPeriod = 'month';
  viewDate: Date = new Date();
  public offersArray:Array<any> = [];
  events = [];
  groupClicked = false;
  busy: Subscription;

  // select variables
  manufacturers = [];
  categories = [];
  offerTypes = [];
  selectedCategories = [];
  selectedManufacturers = [];
  selectedOfferTypes = [];
  selectedPromotions = [];
  public radioModel: string = 'All';
  mcluURL: string = "";
  availableSources = [];

  //table
  public rows:Array<any> = [];

  public config:any = {
    paging: true,
    filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered']
  };
  public page:number = 1;
  public itemsPerPage:number = 5;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public tableData = [];
  selectedOffer:USCalendarDataModel = new USCalendarDataModel(null);

  constructor(public webservice: WebserviceController, private analyticsService: AnalyticsService,
              public sanitizer: DomSanitizer) {
    this.length = this.tableData.length;
  }
  ngOnInit(){
    this.busy = this.webservice.getNARForecastData().subscribe(response => {
        for (var i = 0; i < response.length; i++){
          let offerToFormat = response[i];
          let event = new USCalendarDataModel(offerToFormat);
          this.offersArray.push(event);
        }
        this.availableSources = this.forecastData.getAvailableSources(this.offersArray);
        this.today();
        this.updateData();
      },
      error => {
        this.errorComponent.showErrorModalForError(error);
      });
  }

  increment(): void {
    this.changeDate(addPeriod(this.view, this.viewDate, 1));
  }

  decrement(): void {
    this.changeDate(subPeriod(this.view, this.viewDate, 1));
  }

  today(): void {
    this.changeDate(new Date());
  }

  changeDate(date: Date): void {
    this.viewDate = date;
  }

  dayClicked({date, events}: {date: Date, events: CalendarEvent[]}): void {
    if(this.groupClicked == false) {
      var dataArray = this.forecastData.getDataObjects(this.offersArray);
      this.categories = this.forecastData.getCategoryArray(dataArray, this.radioModel, this.selectedManufacturers, this.selectedOfferTypes);
      this.manufacturers = this.forecastData.getManufacturersArray(dataArray, this.radioModel, this.selectedCategories, this.selectedOfferTypes);
      this.offerTypes = this.forecastData.getOfferTypesArray(dataArray, this.radioModel, this.selectedCategories, this.selectedManufacturers);
      dataArray = this.forecastData.filterSource(dataArray, this.radioModel);
      dataArray = this.forecastData.filterCategories(dataArray, this.selectedCategories);
      dataArray = this.forecastData.filterManufacturer(dataArray, this.selectedManufacturers);
      dataArray = this.forecastData.filterOfferTypes(dataArray, this.selectedOfferTypes);
      
      if(this.selectedPromotions && this.selectedPromotions.length > 0) {
        var results = [];
        this.selectedPromotions.forEach(key => {
          var values = dataArray.filter(evt => evt["id"]==key);
          if(values) {
            values.forEach(val => results.push(val));
          } else {
            console.log("Did not find key "+key);
          }
        });
        dataArray = results;
      }

      this.tableData = this.forecastTableData.getCouponTableData(dataArray, date, null, false);
      if (this.tableData.length > 0) {
        this.selectedOffer = this.tableData[0];
        this.mcluURL = getMCLUBaseURL() + this.selectedOffer.mclu + "&parentcorpkey=" + this.selectedOffer.parentCorpKey;
        this.showChildModal();
      }
    }
  }
  updateData(){
    console.log("Entering updateData()");
    var dataArray = this.forecastData.getDataObjects(this.offersArray);
    this.categories = this.forecastData.getCategoryArray(dataArray, this.radioModel, this.selectedManufacturers, this.selectedOfferTypes);
    this.manufacturers = this.forecastData.getManufacturersArray(dataArray, this.radioModel, this.selectedCategories, this.selectedOfferTypes);
    this.offerTypes = this.forecastData.getOfferTypesArray(dataArray, this.radioModel, this.selectedCategories, this.selectedManufacturers);
    dataArray = this.forecastData.filterSource(dataArray, this.radioModel);
    dataArray = this.forecastData.filterCategories(dataArray, this.selectedCategories);
    dataArray = this.forecastData.filterManufacturer(dataArray, this.selectedManufacturers);
    dataArray = this.forecastData.filterOfferTypes(dataArray, this.selectedOfferTypes);

    if(this.selectedPromotions && this.selectedPromotions.length > 0) {
      var results = [];
      this.selectedPromotions.forEach(key => {
        var values = dataArray.filter(evt => evt["id"]==key);
        if(values) {
          values.forEach(val => results.push(val));
         } else {
          console.log("Did not find key "+key);
         }
      });
      dataArray = results;
    }
    this.events = dataArray;

  }

  eventClicked(group): void {
    this.groupClicked = true;
    let data = group[1];
    this.selectedOffer = data[0];
    this.tableData = this.forecastTableData.getCouponTableData(data, this.selectedOffer.start, null, false);
    this.mcluURL = getMCLUBaseURL() + this.selectedOffer.mclu + "&parentcorpkey=" + this.selectedOffer.parentCorpKey;
    this.showChildModal();
  }
//Select

  public refreshSourceNames(sourceNames:any):void {
    this.selectedManufacturers = sourceNames;
    this.updateData();
  }
  public refreshCategory(categories:any):void {
    this.selectedCategories = categories;
    this.updateData();
  }
  public refreshOfferTypes(types:any):void {
    this.selectedOfferTypes = types;
    this.updateData();
  }

  public selectedCategory(value:any):void {
    this.analyticsService.sendEvent("Added Category Filter", value.id);
  }
  public removedCategory(value:any):void {
    this.analyticsService.sendEvent("Removed Category Filter", value.id);
  }
  public selectedManufacturer(value:any):void {
    this.analyticsService.sendEvent("Added Manufacturer Filter", value.id);
  }
  public removedManufacturer(value:any):void {
    this.analyticsService.sendEvent("Removed Manufacturer Filter", value.id);
  }
  public selectedOfferType(value:any):void {
    this.analyticsService.sendEvent("Added Offer Type Filter", value.id);
  }
  public removedOfferType(value:any):void {
    this.analyticsService.sendEvent("Removed Offer Type Filter", value.id);
  }

  public showChildModal():void {
    this.onChangeTable(this.config);
    this.childModal.show();

  }

  public hideChildModal():void {
    this.page = 1;
    this.groupClicked = false;
    this.childModal.hide();
  }
  //Table
  public changePage():Array<any> {
    let start = (this.page - 1) * this.itemsPerPage;
    let end = this.itemsPerPage > -1 ? (start + this.itemsPerPage) : this.tableData.length;
    return this.tableData.slice(start, end);
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    this.rows = page && config.paging ? this.changePage() : this.tableData;
    this.length = this.tableData.length;
  }
  public onCellClick(data: any): any {
    this.selectedOffer = data;
    this.mcluURL = getMCLUBaseURL() + this.selectedOffer.mclu + "&parentcorpkey=" + this.selectedOffer.parentCorpKey;
  }

  beforeMonthViewRender({body}: {body: CalendarMonthViewDay[]}): void {
    body.forEach((cell) => {
      const groups: any = {};
      cell.events.forEach((event: MyEvent) => {
        groups[event.source] = groups[event.source] || [];
        groups[event.source].push(event);
      });

      cell['eventGroups'] = (<any>Object).entries(groups);

      let calendarDate = cell.date;
      var count = 0;
      for (var i = 0; i < this.events.length; i++) {
        let event = this.events[i];
        if (calendarDate >= event.start && calendarDate <= event.endDate) {
          count++;
        }
      }
      cell.badgeTotal = count;
    });
  }
  getColorForSource(group){
    let source = group[1][0].source;
    if (source == 'Retailer'){
      return '#a5da00';
    }
    else if (source == 'Manufacturer'){
      return '#fc6921'
    }
    else{
      return '#4000ec';

    }
  }
  getBackgroundColorForSource(source:string){
    if(source == "All"){
      return '#1A74BA';
    }
    else if(source == "Manufacturer"){
      return '#fc6921'
    }
    else if(source == "Retailer"){
      return '#a5da00';
    }
    else {
      return '#4000ec';
    }
  }


  handleHierarchyChanged(evt) {
    console.log("Entering handleHierarchyChanged ",evt);
    this.selectedPromotions=evt;
    this.updateData();
  }

}
