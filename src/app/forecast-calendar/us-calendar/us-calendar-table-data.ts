/**
 * Created by jmuzio on 7/10/17.
 */

import {DatePipe} from "@angular/common";
/**
 * Created by jmuzio on 2/20/17.
 */
export class USCalendarTableData {
  getCouponTableData(tableData:Array<any>, selectedDate: Date, selectedManufacturer: string, startsTodayOnly: boolean) {

    var dataObj = [];
    var tableDataArray = [];
    var datePipe = new DatePipe('en-US');
    for (var i = 0; i < tableData.length; i++) {
      let offer = tableData[i];
      let offerStartDate = new Date(offer.start);
      let offerStopDate = new Date(offer.endDate);
      var filteredData = [];
      if (offerStartDate <= selectedDate && offerStopDate >= selectedDate){
        if (this.containsMCLU(dataObj, offer.mclu) == false){
          dataObj.push(offer);
        }
      }
      if (selectedManufacturer != null){
        for (var j = 0; j < dataObj.length; j++){
          let data = dataObj[j];
          if (data.manufacturer == selectedManufacturer){
            filteredData.push(data);
          }
        }
        dataObj = filteredData;
        filteredData = [];
      }
      if (startsTodayOnly == true){
        for (var j = 0; j < dataObj.length; j++){
          let data = dataObj[j];
          if (this.dateEquals(selectedDate, data.start)){
            filteredData.push(data);
          }
        }
        dataObj = filteredData;
        filteredData = [];
      }
    }
    for (var j = 0; j < dataObj.length; j++){
      let data = dataObj[j];
      let tableObj = {
        manufacturer: data.manufacturer,
        category: data.category,
        startDate: datePipe.transform(data.start),
        stopDate: datePipe.transform(data.endDate),
        mclu: data.mclu,
        type: data.type,
        value: data.value,
        parentCorpKey: data.parentCorpKey
      };
      tableDataArray.push(tableObj);
    }
    return tableDataArray;
  }
  containsMCLU(array:Array<any>, MCLU:string):boolean{
    for (var i = 0; i < array.length; i++){
      let objToCheck = array[i];
      if (MCLU == objToCheck.mclu){
        return true;
      }
    }
    return false
  }
  containsManufacturer(array:Array<any>, manufacturer:string):boolean{
    for (var i = 0; i < array.length; i++){
      let objToCheck = array[i];
      if (manufacturer == objToCheck.manufacturer){
        return true;
      }
    }
    return false
  }
  dateIsBetween = function(dateToCheck:Date, startDate:Date, endDate: Date): boolean{
    if (dateToCheck >= startDate && dateToCheck <= endDate){
      return true;
    }
    else{
      return false;
    }
  };
  dateEquals = function(dateToCheck: Date, date:Date): boolean {
    if (dateToCheck.getFullYear() == date.getFullYear() &&
      dateToCheck.getMonth() == date.getMonth() &&
      dateToCheck.getDay() == date.getDay()) {
      return true;
    }
    else {
      return false;
    }
  }
}


