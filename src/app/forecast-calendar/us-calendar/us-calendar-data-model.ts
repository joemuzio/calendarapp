/**
 * Created by jmuzio on 4/5/17.
 */
export class USCalendarDataModel{
  id:string;
  title:string;
  manufacturer:string;
  category:string;
  start:Date;
  endDate:Date;
  color:{};
  source:string;
  mclu:string;
  value:number;
  type:string;
  couponType:string;
  parentCorpKey:string;
  isActive:boolean = true;
  constructor(offer:any){
    if(offer != null) {
      this.id = offer['promo_key'];
      this.manufacturer = offer['mfg_nm'];
      this.title = this.manufacturer;
      this.category = this.formatCategoryText(offer['cmc_cat_descr']);
      this.start = new Date(offer['start_dt']);
      this.endDate = new Date(offer['stop_dt']);
      this.source = offer['custcol_80'];
      this.mclu = offer['mclu_nbr'];
      this.value = offer['coup_val_amt'];
      this.type = offer['custcol_85'];
      this.couponType = offer['custcol_81'];
      this.parentCorpKey = offer['parent_corp_key'];
      this.color = this.getColorForSource();
      if(offer['clu_on_flg'] == 'N' || offer['clu_on_flg'] == 'n'){
        this.isActive = false;
      }
    }
  }
  
  formatCategoryText(categoryString:string):string{
    return (!categoryString)?categoryString:categoryString.replace("/", " / ");
  }

  SOURCE_COLORS:{"Retailer":{primary:"#a5da00"},"Free Bank":{primary:"#4000ec"},"Other":{primary:"#fc6921"}};
  getColorForSource(){
    if (this.source) {
      return this.SOURCE_COLORS[this.source];
    } else {
      return {primary: '#fc6921'}
    }
  }
}
