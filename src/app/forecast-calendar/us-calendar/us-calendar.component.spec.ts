import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsCalendarComponent } from './us-calendar.component';

describe('UsCalendarComponent', () => {
  let component: UsCalendarComponent;
  let fixture: ComponentFixture<UsCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
