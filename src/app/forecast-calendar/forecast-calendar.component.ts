import {OnInit, Component} from '@angular/core';
import {WebserviceController} from "../WebserviceController";
import {AnalyticsService} from "../AnalyticsService";

@Component({
  selector: 'app-forecast-calendar',
  templateUrl: './forecast-calendar.component.html',
  styleUrls: ['./forecast-calendar.component.css']
})

export class ForecastCalendarComponent implements OnInit {

  retailerCountry = "";
  constructor(public webservice: WebserviceController, private analyticsService:AnalyticsService) {
  }
  ngOnInit(){
    this.webservice.getUserData().subscribe(response => {
        if(response.retailer.id != 12) {
          this.retailerCountry = "US";
        }
        else {
          this.retailerCountry = "UK";
        }
      },
      err => {
        console.log(err)
      });
  }
}
