import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HierFilterComponent } from './hier-filter.component';

describe('HierFilterComponent', () => {
  let component: HierFilterComponent;
  let fixture: ComponentFixture<HierFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HierFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HierFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
