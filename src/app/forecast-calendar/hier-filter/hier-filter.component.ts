import {Component, OnInit, EventEmitter, Output} from '@angular/core';

import {environment} from '../../../environments/environment';
import {WebserviceController} from "../../WebserviceController";
import {PromoHierarchyItem,HierIndex} from '../../app.models';

@Component({
  selector: 'app-hier-filter',
  templateUrl: './hier-filter.component.html',
  styleUrls: ['./hier-filter.component.css']
})
export class HierFilterComponent implements OnInit {

  @Output("SelectionChanged")
  public selectionChanged:EventEmitter<any>=new EventEmitter<any>();

  public hierLvl:number = 2;
  public brands:any[];
  public selectedBrands:any[];
  private hierarchyLevels:PromoHierarchyItem[];
  private hier1:HierIndex[];
  private hier2:HierIndex[];
  private hier3:HierIndex[];
  private hier4:HierIndex[];
  private hier5:HierIndex[];

  constructor(public webservice: WebserviceController) { }

  ngOnInit() {
    console.log("Entering hier-filter ngOnInit()");
    this.webservice.getPromoHierData().subscribe(
      r => {
        this.parsePromoHierData(r);
      },
      e => {
        console.log("Error retrieving promotion product hierarchy.", e);
      }
    );
  }

  parsePromoHierData(data:PromoHierarchyItem[]) {
    console.log("Starting to parse product hierarchy.");
    if(!data) return;
    this.hierarchyLevels=data;
    this.brands=this.getHierarchyLevel(this.hierLvl);
    console.log("Exiting parse product hierarchy.");
  }

  public getHierarchyLevel(lvl:number):HierIndex[] {
      if(lvl==1) return this.hier1;
      if(lvl==2) return this.hier2; 
      if(lvl==3) return this.hier3; 
      if(lvl==4) return this.hier4; 
      if(lvl==5) return this.hier5; 
      return null;
  }

  public setHierarchyLevel(lvl:number, data:HierIndex[]):void {
    if(lvl==1) this.hier1 = data;
    if(lvl==2) this.hier2 = data; 
    if(lvl==3) this.hier3 = data; 
    if(lvl==4) this.hier4 = data; 
    if(lvl==5) this.hier5 = data; 
}

  parseHierLevel(lvl:number):HierIndex[] {
    console.log("Check if hierarchy bucket filled.");
    let arHier:HierIndex[]=this.getHierarchyLevel(lvl);
    if(!arHier) {
      arHier = new Array<HierIndex>();
    } else {
      if(arHier.length>0) return arHier;
    }

    console.log("Fill hashmap for level "+lvl);
    let hierObj = {};
    let idKey:string="l"+lvl+"_hier_cd";
    let descKey:string="l"+lvl+"_hier_desc";
    for(var i=0;i<this.hierarchyLevels.length;i++) {
      let item:PromoHierarchyItem=this.hierarchyLevels[i];
      let code:string=item[idKey];
      if(code=="-1") continue;

      let l1:HierIndex = <HierIndex>hierObj[code];
      if(!l1) {
        l1=new HierIndex();
        l1.id = code;
        l1.text = item[descKey];
        l1.keys.push(item.promo_key);
        hierObj[code]=l1;
      } else {
        l1.keys.push(item.promo_key);
      }
    }

    console.log("Extract hash keys for level "+lvl);
    let n:string;
    for (n in hierObj) {
      arHier.push(hierObj[n]);
    }

    this.setHierarchyLevel(lvl, arHier);
    hierObj=null;
    return arHier;
  }

  public levelChanged(evt:any):void {
    this.selectedBrands = [];
    this.brands=this.parseHierLevel(this.hierLvl);
  }

  public refreshBrands(data:any[]):void {
    console.log("Entering refresh brands ", data);
    let promoKeys=[];
    let id:string="l"+this.hierLvl+"_hier_cd";
    data.forEach(brand => {
      let lvl:PromoHierarchyItem[]=this.hierarchyLevels.filter(item => item[id]==brand.id);
      lvl.forEach(promo => {
        if(!promoKeys[promo.promo_key]) promoKeys.push(promo.promo_key);
      });
    });
    this.selectionChanged.next(promoKeys);
  }
  
  public selectedBrand(value:any) {
    console.log("Selected Brand ", value);
    let item:HierIndex=this.getHierarchyLevel(this.hierLvl).filter(r => r.id==value.id)[0];
    this.selectedBrands.push(item);
  }

  public removedBrand(value:any) {
    console.log("Removed Brand ", value);
    this.selectedBrands=this.selectedBrands.filter(r => r.id!=value.id);
  }


}
