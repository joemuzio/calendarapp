/**
 * Created by jmuzio on 4/5/17.
 */
export class UKCalendarDataModel{
  title:string;
  manufacturer:string;
  category:string;
  start:Date;
  endDate:Date;
  color:{};
  source:string;
  sourceCode:number;
  mclu:string;
  value:number;
  type:string;
  couponType:string;
  parentCorpKey:string;
  businessArea:string;
  brand:string;
  campaignDescription:string;
  constructor(offer:any){
    if(offer != null) {
      this.manufacturer = offer['parent_corp_nm'];
      this.title = this.manufacturer;
      this.category = this.formatCategoryText(offer['cmc_cat_descr']);
      this.start = new Date(offer['start_dt']);
      this.endDate = new Date(offer['stop_dt']);
      this.sourceCode = offer['d_order_nbr'];
      this.source = this.getSourceForSourceCode();
      this.mclu = offer['mclu_nbr'];
      this.value = offer['coup_val_amt'];
      this.type = offer['custcol_85'];
      this.couponType = offer['custcol_81'];
      this.parentCorpKey = offer['parent_corp_key'];
      this.businessArea = offer['f_business_area'];
      this.brand = offer['d_brand'];
      this.campaignDescription = offer['mclu_descr'];
      this.color = this.getColorForSource();
    }
  }
  formatCategoryText(categoryString:string):string{
    var updatedCategoryString = categoryString.replace("/", " / ");
    return updatedCategoryString;
  }
  getSourceForSourceCode(){
    if(this.sourceCode == 19265){
      return "Local";
    }
    else if(this.sourceCode == 16185 || this.sourceCode == 13266 || this.sourceCode == 13267){
      return "Retail";
    }
    else{
      return "Supplier";
    }
  }
  getColorForSource(){
    if (this.source == 'Retail'){
      return {
        primary: '#a5da00',
        // secondary: '#ffffff'
      };
    }
    else if(this.source == 'Local'){
      return {
        primary: '#fc6921',
        // secondary: '#cadabd'
      }
    }
    else{
      return {
        primary: '#1a74ba',
        // secondary: '#cadabd'
      }
    }
  }
}
