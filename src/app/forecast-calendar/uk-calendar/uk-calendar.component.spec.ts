import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UkCalendarComponent } from './uk-calendar.component';

describe('UkCalendarComponent', () => {
  let component: UkCalendarComponent;
  let fixture: ComponentFixture<UkCalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UkCalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UkCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
