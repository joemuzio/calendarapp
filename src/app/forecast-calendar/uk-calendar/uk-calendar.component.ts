import {OnInit, Component, Input, ViewChild} from '@angular/core';
import { CalendarEvent, CalendarMonthViewDay } from 'angular-calendar';
import {subMonths, addMonths, addDays, addWeeks, subDays, subWeeks} from 'date-fns';
import {WebserviceController} from "../../WebserviceController";
import { ModalDirective } from 'ngx-bootstrap/modal';
import {UKCalendarData} from "./uk-calendar-data";
import {UKCalendarTableData} from "./uk-calendar-table-data";
import {UKCalendarDataModel} from "./uk-calendar-data-model";
import {Subscription} from 'rxjs';
import {ErrorComponent} from "../../error/error.component";
import {AnalyticsService} from "../../AnalyticsService";
import {DomSanitizer} from "@angular/platform-browser";
import { environment } from '../../../environments/environment';


type CalendarPeriod = 'day' | 'week' | 'month';

function addPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: addDays,
    week: addWeeks,
    month: addMonths
  }[period](date, amount);
}

function subPeriod(period: CalendarPeriod, date: Date, amount: number): Date {
  return {
    day: subDays,
    week: subWeeks,
    month: subMonths
  }[period](date, amount);
}
function getMCLUBaseURL(){
  if(environment.production){
    return "https://insights-service.catalinamarketing.com/BIImageService/imageservice/image?country=UK&mclu=";
  }
  else{
    return "https://uatinsights.catalina.com/BIImageService/imageservice/image?country=UK&mclu=";
  }
}
interface MyEvent extends CalendarEvent {
  manufacturer: string;
  source:string;
  color: {primary: string, secondary: string}
}
@Component({
  selector: 'app-uk-calendar',
  templateUrl: './uk-calendar.component.html',
  styleUrls: ['./uk-calendar.component.css']
})
export class UkCalendarComponent implements OnInit {
  @ViewChild('childModal') public childModal:ModalDirective;
  @ViewChild(ErrorComponent) public errorComponent: ErrorComponent;
  public forecastData = new UKCalendarData();
  public forecastTableData = new UKCalendarTableData();
  view: CalendarPeriod = 'month';
  viewDate: Date = new Date();
  public offersArray:Array<any> = [];
  events = [];
  groupClicked = false;
  busy: Subscription;

  // select variables

  businessAreas = [];
  brands = [];
  selectedBusinessAreas = [];
  selectedBrands = [];

  public radioModel: string = 'All';
  mcluURL: string = "";

  //table
  public rows:Array<any> = [];
  public config:any = {
    paging: true,
    filtering: {filterString: ''},
    className: ['table-striped', 'table-bordered']
  };
  public page:number = 1;
  public itemsPerPage:number = 5;
  public maxSize:number = 5;
  public numPages:number = 1;
  public length:number = 0;

  public tableData = [];
  selectedOffer:UKCalendarDataModel = new UKCalendarDataModel(null);

  constructor(private webservice: WebserviceController, private analyticsService: AnalyticsService,
              public sanitizer: DomSanitizer) {
    this.length = this.tableData.length;
  }
  ngOnInit(){
    this.busy = this.webservice.getNARForecastDataUK().subscribe(response => {
        for (var i = 0; i < response.length; i++){
          let offerToFormat = response[i];
          let event = new UKCalendarDataModel(offerToFormat);
          this.offersArray.push(event);
        }
        this.today();
        this.updateData();
      },
      error => {
        this.errorComponent.showErrorModalForError(error);
      });
  }

  increment(): void {
    this.changeDate(addPeriod(this.view, this.viewDate, 1));
  }

  decrement(): void {
    this.changeDate(subPeriod(this.view, this.viewDate, 1));
  }

  today(): void {
    this.changeDate(new Date());
  }

  changeDate(date: Date): void {
    this.viewDate = date;
  }

  dayClicked({date, events}: {date: Date, events: CalendarEvent[]}): void {
    if(this.groupClicked == false) {
      var dataArray = this.forecastData.getDataObjects(this.offersArray);
      this.businessAreas = this.forecastData.getBusinessAreaArray(dataArray, this.radioModel, this.selectedBrands);
      this.brands = this.forecastData.getBrandArray(dataArray,  this.radioModel, this.selectedBusinessAreas);
      dataArray = this.forecastData.filterSource(dataArray, this.radioModel);
      dataArray = this.forecastData.filterBusinessArea(dataArray, this.selectedBusinessAreas);
      dataArray = this.forecastData.filterBrand(dataArray, this.selectedBrands);
      this.tableData = this.forecastTableData.getCouponTableData(dataArray, date, null, false);
      if (this.tableData.length > 0) {
        this.mcluURL = getMCLUBaseURL() + this.tableData[0].mclu;
        this.selectedOffer = this.tableData[0];
        this.showChildModal();
      }
    }
  }
  updateData(){
    var dataArray = this.forecastData.getDataObjects(this.offersArray);
    this.businessAreas = this.forecastData.getBusinessAreaArray(dataArray, this.radioModel, this.selectedBrands);
    this.brands = this.forecastData.getBrandArray(dataArray,  this.radioModel, this.selectedBusinessAreas);
    dataArray = this.forecastData.filterSource(dataArray, this.radioModel);
    dataArray = this.forecastData.filterSource(dataArray, this.radioModel);
    dataArray = this.forecastData.filterBusinessArea(dataArray, this.selectedBusinessAreas);
    dataArray = this.forecastData.filterBrand(dataArray, this.selectedBrands);

    this.events = dataArray;

  }

  eventClicked(group): void {
    this.groupClicked = true;
    let data = group[1];
    let firstCoupon = data[0];
    this.tableData = this.forecastTableData.getCouponTableData(data, firstCoupon.start, null, false);
    this.mcluURL = getMCLUBaseURL() + firstCoupon.mclu;
    this.selectedOffer = firstCoupon;

    this.showChildModal();
  }
//Select

  public refreshBusinessAreas(areas:any):void {
    this.selectedBusinessAreas = areas;
    this.updateData();
  }
  public refreshBrands(brandsArray:any):void {
    this.selectedBrands = brandsArray;
    this.updateData();
  }
  public selectedBusinessArea(value:any):void{

  }
  public selectedBrand(value:any):void{

  }
  public removedBusinessArea(value:any):void{

  }
  public removedBrand(value:any):void{

  }

  public showChildModal():void {
    this.onChangeTable(this.config);
    this.childModal.show();

  }

  public hideChildModal():void {
    this.page = 1;
    this.groupClicked = false;
    this.childModal.hide();
  }
  //Table
  public changePage():Array<any> {
    let start = (this.page - 1) * this.itemsPerPage;
    let end = this.itemsPerPage > -1 ? (start + this.itemsPerPage) : this.tableData.length;
    return this.tableData.slice(start, end);
  }

  public onChangeTable(config:any, page:any = {page: this.page, itemsPerPage: this.itemsPerPage}):any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }
    this.rows = page && config.paging ? this.changePage() : this.tableData;
    this.length = this.tableData.length;
  }
  public onCellClick(data: any): any {
    this.mcluURL = getMCLUBaseURL() + data.mclu;
    this.selectedOffer = data;

  }

  beforeMonthViewRender({body}: {body: CalendarMonthViewDay[]}): void {
    body.forEach((cell) => {
      const groups: any = {};
      cell.events.forEach((event: MyEvent) => {
        groups[event.source] = groups[event.source] || [];
        groups[event.source].push(event);
      });

      cell['eventGroups'] = (<any>Object).entries(groups);

      let calendarDate = cell.date;
      var count = 0;
      for (var i = 0; i < this.events.length; i++) {
        let event = this.events[i];
        if (calendarDate >= event.start && calendarDate <= event.endDate) {
          count++;
        }
      }
      cell.badgeTotal = count;
    });
  }
  getColorForSource(group){
    let source = group[1][0].source;
    if (source == 'Local'){
      return '#a5da00';
    }
    else if (source == 'Retail'){
      return '#fc6921';
    }
    else if (source == 'Supplier'){
      return '#4000ec';
    }
  }
}
