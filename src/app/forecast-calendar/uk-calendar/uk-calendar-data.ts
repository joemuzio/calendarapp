/**
 * Created by jmuzio on 4/5/17.
 */
import {UKCalendarDataModel} from "./uk-calendar-data-model";

export class UKCalendarData {

  public containsBrand(brand: string, brandsArray:Array<any>): boolean{
    let doesContain = false;
    for (var i = 0; i < brandsArray.length; i++){
      let brandString = brandsArray[i].id;
      if (brand == brandString){ doesContain = true; }
    }
    return doesContain;
  }
  public containsBusinessArea(businessArea: string, businessAreaArray:Array<any>): boolean{
    let doesContain = false;
    for (var i = 0; i < businessAreaArray.length; i++){
      let businessAreaString = businessAreaArray[i].id;
      if (businessArea == businessAreaString){ doesContain = true; }
    }
    return doesContain;
  }
  private containsMCLU(mclu:string, array):boolean{
    for (var i = 0; i < array.length; i++){
      let offerToCheck = array[i];
      if (offerToCheck.mclu == mclu){
        return true;
      }
    }
    return false;
  }


  sameDaySameManufacturer(event:UKCalendarDataModel, array:Array<any>):boolean{
    for(var i = 0; i < array.length; i++){
      let compareEvent = array[i];
      if (this.sameDay(event, compareEvent)){
        if(compareEvent.manufacturer == event.manufacturer){
          return true;
        }
      }
    }
    return false
  }
  sameDay(event1:UKCalendarDataModel, event2:UKCalendarDataModel):boolean{
    if(event1.start.getFullYear() == event2.start.getFullYear() &&
      event1.start.getMonth() == event2.start.getMonth() &&
      event1.start.getDay() == event2.start.getDay()){
      return true;
    }
    return false;
  }

  public getDataObjects(offersArray:Array<any>):Array<any>{
    var newEvents = [];
    for (var i = 0; i < offersArray.length; i++){
      let event = offersArray[i];
      var containsMCLU = this.containsMCLU(event.mclu, newEvents);
      if (containsMCLU == false){
        newEvents.push(event);
      }
    }
    return newEvents;
  }

  filterBrand(dataArray:Array<any>, selectedArray:Array<any>): Array<any>{
    if(selectedArray.length > 0) {
      var newData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let obj = dataArray[i];
        for (var j = 0; j < selectedArray.length; j++) {
          let selected = selectedArray[j];
          if (selected.id == obj.brand) {
            newData.push(obj);
          }
        }
      }
      return newData;
    }
    else{
      return dataArray;
    }
  }

  filterBusinessArea(dataArray:Array<any>, selectedArray:Array<any>): Array<any>{
    if(selectedArray.length > 0) {
      var newData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let obj = dataArray[i];
        for (var j = 0; j < selectedArray.length; j++) {
          let selected = selectedArray[j];
          if (selected.id == obj.businessArea) {
            newData.push(obj);
          }
        }
      }
      return newData;
    }
    else{
      return dataArray;
    }
  }


  filterSource(dataArray:Array<any>, radioModel: string):Array<any>{
    if (radioModel != "All") {
      var newData = [];
      for (var i = 0; i < dataArray.length; i++) {
        let obj = dataArray[i];
        if (radioModel == "Retail"){
          if (obj.source == "Retail"){
            newData.push(obj);
          }
        }
        else if(radioModel == "Local"){
          if (obj.source == "Local"){
            newData.push(obj);
          }
        }
        else if(radioModel == "Supplier"){
          if (obj.source == "Supplier"){
            newData.push(obj);
          }
        }
      }
      return newData;
    }
    else{
      return dataArray;
    }
  }
  getOfferCountForDay(calendarDate: Date, offersArray:Array<UKCalendarDataModel>):number{
    var count = 0;
    for (var i = 0; i < offersArray.length; i++) {
      let event = offersArray[i];
      if (calendarDate >= event.start && calendarDate <= event.endDate) {
        count++;
      }
    }
    return count;
  }

  getBrandArray(dataArray:Array<any>, radioModel:string, selectedBusinessArea:Array<any>):Array<any>{
    var brandsArray = [];
    dataArray = this.filterSource(dataArray, radioModel);
    dataArray = this.filterBusinessArea(dataArray, selectedBusinessArea);
    for (var i = 0; i < dataArray.length; i++){
      let offer = dataArray[i];
      if (this.containsBrand(offer.brand, brandsArray) == false) {
        let categoryObj = {
          id: offer.brand,
          text: '<span style="pointer-events:none">' + offer.brand + '</span>'
        };
        brandsArray.push(categoryObj);
      }
    }
    brandsArray.sort(function(a, b) {
      var textA = a.text.toUpperCase();
      var textB = b.text.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    return brandsArray;
  }

  getBusinessAreaArray(dataArray:Array<any>, radioModel:string, selectedBrands:Array<any>):Array<any>{
    var businessAreaArray = [];
    dataArray = this.filterSource(dataArray, radioModel);
    dataArray = this.filterBrand(dataArray, selectedBrands);
    for (var i = 0; i < dataArray.length; i++){
      let offer = dataArray[i];
      if (this.containsBusinessArea(offer.businessArea, businessAreaArray) == false) {
        let categoryObj = {
          id: offer.businessArea,
          text: '<span style="pointer-events:none">' + offer.businessArea + '</span>'
        };
        businessAreaArray.push(categoryObj);
      }
    }
    businessAreaArray.sort(function(a, b) {
      var textA = a.text.toUpperCase();
      var textB = b.text.toUpperCase();
      return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
    });
    return businessAreaArray;
  }


}
