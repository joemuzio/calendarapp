import {Component, OnInit, ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import {WebserviceController} from "../WebserviceController";
import {Subscription} from 'rxjs';
import {ErrorComponent} from "../error/error.component";
import {AnalyticsService} from "../AnalyticsService";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  busy: Subscription;
  @ViewChild(ErrorComponent) errorComponent: ErrorComponent;
  private iOS;
  constructor(private webservice: WebserviceController, private router: Router, private analyticsService: AnalyticsService) {}

  login(event, username, password) {
    if(this.iOS == false || this.iOS >= 10) {
      this.busy = this.webservice.login(username, password).subscribe(
        response => {
          this.webservice.setUserName(username);
          this.webservice.setToken(response['access_token']);
          this.busy = this.webservice.getUserData().subscribe(
            response => {
              if (response.retailer != null && response.retailer.id != "2") {
                this.router.navigate(['home']);
              }
            },
            error => {
              this.errorComponent.showErrorModalForNoRetailerAssigned();
              // no retailer
            }
          );
        },
        error => {
          this.errorComponent.showErrorModalForError(error);
        }
      );
    }
    else{
        this.errorComponent.showiOSTooLowModal();
    }
  }

  ngOnInit() {
    this.iOS = parseFloat(
        ('' + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(navigator.userAgent) || [0,''])[1])
          .replace('undefined', '3_2').replace('_', '.').replace('_', '')
      ) || false;
    if(this.iOS != false && this.iOS < 10){
      this.errorComponent.showiOSTooLowModal();
    }
  }
}
