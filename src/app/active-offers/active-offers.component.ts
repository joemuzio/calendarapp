import {Component, OnInit, ViewChild} from '@angular/core';
import {WebserviceController} from "../WebserviceController";
import {DomSanitizer} from '@angular/platform-browser';
import {ErrorComponent} from "../error/error.component";
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-active-offers',
  templateUrl: './active-offers.component.html',
  styleUrls: ['./active-offers.component.css']
})
export class ActiveOffersComponent implements OnInit {
  @ViewChild(ErrorComponent) errorComponent: ErrorComponent;
  url = "";

  constructor(private webservices: WebserviceController, private sanitizer: DomSanitizer) {

  }

  ngOnInit() {

    this.webservices.getNARDistributionURLAndToken().subscribe(
      response => {
        if(response[0].retailer.documentId && response[1].data.token) {
          let documentId = response[0].retailer.documentId;
          let mstrToken = response[1].data.token;
          let userId = localStorage.getItem('user');
          if (environment.production) {
            this.url = "https://insights.catalina.com/CI/servlet/mstrWeb" +
              "?evt=3140&src=mstrWeb.3140&documentId=" + documentId + "&Server=PMICROSTILV03" +
              "&Project=Catalina%20Insights&Port=0&share=1&hiddensections=header,path,dockTop,dockLeft,footer" +
              "vismode=0&currentViewMedia=8&mstrwid=1&loginReq=true&Uid=" + userId + "&cmcID=" + mstrToken;
          }
          else {
            this.url = "https://uatinsights.catalina.com/CI/servlet/mstrWeb" +
              "?evt=3140&src=mstrWeb.3140&documentId=" + documentId + "&Server=ORLUBIAPPLV02" +
              "&Project=Catalina%20Insights%20-%20UAT&Port=0&share=1&hiddensections=header,path,dockTop,dockLeft,footer" +
              "vismode=0&currentViewMedia=8&mstrwid=1&loginReq=true&Uid=" + userId + "&cmcID=" + mstrToken;
          }
        }
        else{
          this.errorComponent.showErrorModalNoNARIDFound();
        }
      },
      error => {
        this.errorComponent.showErrorModalForError(error);
      }
    );
  }
  transform() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.url);
  }
}
