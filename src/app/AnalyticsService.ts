/**
 * Created by jmuzio on 5/15/17.
 */
import {Injectable} from "@angular/core";
import {NavigationEnd} from "@angular/router";
import {WebserviceController} from "./WebserviceController";

declare let ga: any;

@Injectable()
export class AnalyticsService {
  constructor(private webservice:WebserviceController){}
  public emitEvent(eventCategory: string,
                   eventAction: string,
                   eventLabel: string = null,
                   eventValue: number = null) {
    ga('send', 'event', {
      eventCategory: eventCategory,
      eventLabel: eventLabel,
      eventAction: eventAction,
      eventValue: eventValue
    });
  }
  sendPageViewData(event: NavigationEnd){
    ga('set', 'page', event.urlAfterRedirects);
    ga('send', 'pageview');
  }
  sendRetailerName(retailerName: string){
    ga('set', 'dimension1', retailerName);
  }
  sendEvent(eventCategory:string, eventAction:string){
    this.emitEvent(eventCategory, eventAction);
  }

}
