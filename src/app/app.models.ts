export class PromoHierarchyItem {
    promo_key:string;
    l1_hier_cd:string;
    l1_hier_desc:string;
    l2_hier_cd:string;
    l2_hier_desc:string;
    l3_hier_cd:string;
    l3_hier_desc:string;
    l4_hier_cd:string;
    l4_hier_desc:string;
    l5_hier_cd:string;
    l5_hier_desc:string;
}
export class HierIndex {
    id:string;
    text:string;
    keys:string[]=[];
}