import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router'
import { CalendarModule } from 'angular-calendar';
import { AlertModule, AccordionModule, TooltipModule, TabsModule, ModalModule, PaginationModule, ButtonsModule } from 'ngx-bootstrap';
import { SelectModule } from 'ng2-select-compat';
import { BusyModule } from 'angular2-busy';

import { routes } from './app.routes';
import { AuthGuard } from './auth.guard';
import { AnalyticsService } from './AnalyticsService'
import { AppComponent } from './app.component';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { ActiveOffersComponent } from './active-offers/active-offers.component';
import { WebserviceController } from './WebserviceController';
import { NewUserComponent } from './new-user/new-user.component';
import { ForecastCalendarComponent } from './forecast-calendar/forecast-calendar.component';
import { UsCalendarComponent } from './forecast-calendar/us-calendar/us-calendar.component';
import { UkCalendarComponent } from './forecast-calendar/uk-calendar/uk-calendar.component';
import { ForecastGanttComponent } from './forecast-gantt/forecast-gantt.component';

import {HierDescFilterPipe, HierCodeFilterPipe} from './app.pipes';
import { HierFilterComponent } from './forecast-calendar/hier-filter/hier-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ForecastCalendarComponent,
    ActiveOffersComponent,
    ErrorComponent,
    NewUserComponent,
    UsCalendarComponent,
    UkCalendarComponent,
    ForecastGanttComponent,
    HierDescFilterPipe, 
    HierCodeFilterPipe, 
    HierFilterComponent
  ],
  imports: [
    AlertModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    SelectModule,
    BusyModule,
    AccordionModule.forRoot(),
    CalendarModule.forRoot(),
    BrowserAnimationsModule,
    TooltipModule.forRoot(),
    TabsModule.forRoot(),
    ButtonsModule.forRoot(),
    ModalModule.forRoot(),
    PaginationModule.forRoot(),
    RouterModule.forRoot(routes, {
      useHash: true
    })
  ],
  exports: [
    HierDescFilterPipe, 
    HierCodeFilterPipe
  ],
  providers: [AuthGuard, WebserviceController, AnalyticsService],
  bootstrap: [AppComponent]
})

export class AppModule {

}
